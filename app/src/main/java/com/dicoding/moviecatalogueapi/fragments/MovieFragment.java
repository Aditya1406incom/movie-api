package com.dicoding.moviecatalogueapi.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dicoding.moviecatalogueapi.R;
import com.dicoding.moviecatalogueapi.adapters.MovieAdapter;
import com.dicoding.moviecatalogueapi.model.Movie;
import com.dicoding.moviecatalogueapi.model.MovieRepository;
import com.dicoding.moviecatalogueapi.network.GetMovieCallback;

import java.util.List;

public class MovieFragment extends Fragment {

    private RecyclerView movieList;
    private MovieAdapter movieAdapter;

    private MovieRepository movieRepository;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movie, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        movieRepository = MovieRepository.getInstance();
        movieList = view.findViewById(R.id.rvMovie);
        movieList.setHasFixedSize(true);
        showRecyclerList();

        movieRepository.getMovie(new GetMovieCallback() {
            @Override
            public void onSuccess(List<Movie> movieList) {
                movieAdapter = new MovieAdapter(movieList);
            }

            @Override
            public void onError() {
                Toast.makeText(getContext(), "Please check your connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showRecyclerList() {
        movieList.setLayoutManager(new LinearLayoutManager(getActivity()));
        movieList.setAdapter(movieAdapter);
    }
}