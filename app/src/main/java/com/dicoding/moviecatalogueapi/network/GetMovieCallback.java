package com.dicoding.moviecatalogueapi.network;

import com.dicoding.moviecatalogueapi.model.Movie;

import java.util.List;

public interface GetMovieCallback {
    void onSuccess(List<Movie> movieList);

    void onError();
}
