package com.dicoding.moviecatalogueapi.model;

import com.dicoding.moviecatalogueapi.network.GetMovieCallback;
import com.dicoding.moviecatalogueapi.network.MovieApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieRepository {
    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static final String LANGUAGE = "en-US";
    private static MovieRepository movieRepository;
    private MovieApi movieApi;

    private MovieRepository(MovieApi movieApi) {
        this.movieApi = movieApi;
    }

    public static MovieRepository getInstance() {
        if (movieRepository == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            movieRepository = new MovieRepository(retrofit.create(MovieApi.class));
        }
        return movieRepository;
    }

    public void getMovie(final GetMovieCallback callback) {
        movieApi.getPopularMovies("e2fafa1a7b1a8e2187b1ccd1f09c2679", LANGUAGE, 1)
                .enqueue(new Callback<MovieResponse>() {
                    @Override
                    public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                        if (response.isSuccessful()) {
                            MovieResponse movieResponse = response.body();
                            if (movieResponse != null && movieResponse.getMovieList() != null) {
                                callback.onSuccess(movieResponse.getMovieList());
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<MovieResponse> call, Throwable t) {
                        callback.onError();
                    }
                });
    }
}
